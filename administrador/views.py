from django.shortcuts import render
# Create your views here.


def inicio(request):
    """función para mostrar el inicio del administrador"""
    return render(request, 'base/base_administrador.html', {})
