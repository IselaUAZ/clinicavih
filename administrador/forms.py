from django import forms
from .models import Administrador


class AdministradorForm(forms.models.ModelForm):

    class Meta:

        model = Administrador
        fields = ('medico', 'username', 'password')

        widgets = {'medico':
                   forms.Select(attrs={'class': 'form-control'}),
                   'username':
                       forms.TextInput(attrs={'class': 'form-control'}),
                   'password':
                       forms.TextInput(attrs={'class': 'form-control',
                                              'type': 'password'})}

    def save(self):
        return super().save()
