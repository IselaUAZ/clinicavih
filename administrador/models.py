from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Administrador(User):
    nombre = models.CharField(u'Nombre', max_length=50)
    ap_paterno = models.CharField(u'Paterno', max_length=30)
    ap_materno = models.CharField(u'Materno', max_length=30, null=True)

#Formato para Administrador
    def __str__(self):
        return '{}'.format(self.nombre + ' ' + self.ap_paterno +
                               ' ' + self.ap_materno)
