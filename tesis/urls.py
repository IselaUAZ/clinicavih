"""tesis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from administrador import urls as administrador_urls
from paciente import urls as paciente_urls
from medico import urls as medico_urls
from paciente import urls as paciente_urls
from cita import urls as cita_urls
from login import views

urlpatterns = [
    url(r'^$', views.iniciar_sesion, name='login_usuario'),
    url(r'^cerrar', views.cerrar_sesion, name='cerrar_sesion'),
    url('admin/', admin.site.urls),
    url(r'^medico/', include(medico_urls)),
    url(r'^administrador/', include(administrador_urls)),
    url(r'^paciente/', include(paciente_urls)),
    url(r'^cita/', include(cita_urls)),
]
