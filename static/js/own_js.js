$(document).ready(function () {
    (function ($) {
        $('#filter').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
        })
    }(jQuery));
});

function search_table() {
    var valor = document.getElementById("filter").value;
    document.busqueda.value=valor
}


function ordenarpac(campo){
    var campN = 0
      switch (campo.value){
                  case 'nombre_paciente':
                        campN = 1;
                        break;
                  case 'ap_paterno':
                        campN = 2;
                        break;
                  case 'ap_materno':
                        campN = 3;
                        break;
                  case 'edad':
                        campN = 4;
                        break;
                  case 'institucion':
                        campN = 7;
                        break;
                  default:
                        campN = 0;
      }

      var tbl = document.getElementById("idTablePac").tBodies[0];
      var store = [];
      for(var i=0, len=tbl.rows.length; i<len; i++){
          var row = tbl.rows[i];
          store.push([row.cells[campN].innerText, row])
      }
      store.sort(function(x,y){
          return x[0].toUpperCase() > y[0].toUpperCase();
      });
      for(var i=0, len=store.length; i<len; i++){
          tbl.appendChild(store[i][1]);
      }
      store = null;
}


function OrdenarCit(campo){
    var campN = 0
      switch (campo.value){
                  case 'paciente_fk':
                        campN = 1;
                        break;
                  case 'carga_viral':
                        campN = 2;
                        break;
                  case 'fecha_cita':
                        campN = 3;
                        break;
                  default:
                        campN = 0;
      }

      var tbl = document.getElementById("idTableCit").tBodies[0];
      var store = [];
      for(var i=0, len=tbl.rows.length; i<len; i++){
          var row = tbl.rows[i];
          store.push([row.cells[campN].innerText, row])
      }
      store.sort(function(x,y){
          return x[0].toUpperCase() > y[0].toUpperCase();
      });
      for(var i=0, len=store.length; i<len; i++){
          tbl.appendChild(store[i][1]);
      }
      store = null;
}


function ordenarmed(campo){
    var campN = 0
      switch (campo.value){
                  case 'nombre':
                        campN = 1;
                        break;
                  case 'ap_paterno':
                        campN = 2;
                        break;
                  case 'ap_materno':
                        campN = 3;
                        break;
                  case 'rfc':
                        campN = 4;
                        break;
                  default:
                        campN = 0;
      }

      var tbl = document.getElementById("idTableMedc").tBodies[0];
      var store = [];
      for(var i=0, len=tbl.rows.length; i<len; i++){
          var row = tbl.rows[i];
          store.push([row.cells[campN].innerText, row])
      }
      store.sort(function(x,y){
          return x[0].toUpperCase() > y[0].toUpperCase();
      });
      for(var i=0, len=store.length; i<len; i++){
          tbl.appendChild(store[i][1]);
      }
      store = null;
}


function OrdenarAnt(campo){
    var campN = 0
      switch (campo.value){
                  case 'nombre_paciente':
                        campN = 1;
                        break;
                  case 'ap_paterno':
                        campN = 2;
                        break;
                  case 'ap_materno':
                        campN = 3;
                        break;
                  default:
                        campN = 0;
      }

      var tbl = document.getElementById("idTableAnt").tBodies[0];
      var store = [];
      for(var i=0, len=tbl.rows.length; i<len; i++){
          var row = tbl.rows[i];
          store.push([row.cells[campN].innerText, row])
      }
      store.sort(function(x,y){
          return x[0].toUpperCase() > y[0].toUpperCase();
      });
      for(var i=0, len=store.length; i<len; i++){
          tbl.appendChild(store[i][1]);
      }
      store = null;
}


$('.ver-medico').click(function() {
  var id = $(this).prop('id');

  var nombre = $('#medico-nombre-' + id).html();
  var ap_paterno = $('#medico-appaterno-' + id).html();
  var ap_materno = $('#medico-apmaterno-' + id).html();
  var rfc = $('#medico-rfc-' + id).html();
  var fecha_nacimiento = $('#medico-nacimiento-' + id).html();
  var email = $('#medico-email-' + id).html();

  $('#nombre-medico').html(nombre + ' ' + ap_paterno + ' ' + ap_materno);
  $('#rfc-medico').html(rfc);
  $('#fechanac-medico').html(fecha_nacimiento);
  $('#email-medico').html(email);
});


$('.ver-paciente').click(function() {
    var id = $(this).prop('id');
  
    var nombre_paciente = $('#paciente-nombre-' + id).html();
    var ap_paterno = $('#paciente-appaterno-' + id).html();
    var ap_materno = $('#paciente-apmaterno-' + id).html();
    var edad = $('#paciente-edad-' + id).html();
    var nacimiento = $('#paciente-nacimiento-' + id).html();
    var der_hab = $('#paciente-derhab-' + id).html();
    var institucion = $('#paciente-institucion-' + id).html();
    var ocupacion = $('#paciente-ocupacion-' + id).html();
    var domicilio = $('#paciente-domicilio-' + id).html();
    var est_civil = $('#paciente-estcivil-' + id).html();
    var sexo = $('#paciente-sexo-' + id).html();
    var estado =  $('#paciente-estado-' + id).html();
    var municipio =  $('#paciente-municipio-' + id).html();
  
    $('#nombre-paciente').html(nombre_paciente + ' ' + ap_paterno + ' ' + ap_materno);
    $('#edad-paciente').html(edad);
    $('#fechanac-paciente').html(nacimiento);
    $('#derhab-paciente').html(der_hab);
    $('#institucion-paciente').html(institucion);
    $('#ocupacion-paciente').html(ocupacion);
    $('#domicilio-paciente').html(domicilio);
    $('#estcivil-paciente').html(est_civil);
    $('#sexo-paciente').html(sexo);
    $('#estado-paciente').html(estado);
    $('#municipio-paciente').html(municipio);
  });