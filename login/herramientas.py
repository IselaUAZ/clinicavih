from administrador.models import Administrador

def redireccionar(username):
    try:
        Administrador.objects.get(username=username)
        return '/administrador/inicio'
    except: # noqa E:501
        return '/medico/inicio'
