from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from login.herramientas import redireccionar
from django.shortcuts import redirect


def iniciar_sesion(request):
    user = request.user

    if user is not None and user.is_authenticated and user.is_active:
        redireccion = redireccionar(user.username)
        return HttpResponseRedirect(redireccion)

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = ""

        if username != "" and password != "":
            print(password)
            print(username)
            user = authenticate(username=username, password=password)

        if user is not None:
            redireccion = redireccionar(username)
            login(request, user)
            return HttpResponseRedirect(redireccion)

        else:
            return render(
                request, 'base/login.html',
                {'datos_incorrectos': 'Los datos de inicio de sesión '
                                      'proporcionados no coinciden con ningún '
                                      'usuario registrado.'})
    else:
        return render(
            request, 'base/login.html')


def cerrar_sesion(request):
    logout(request)
    return HttpResponseRedirect('/')
