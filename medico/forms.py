from django import forms
from .models import Medico

class RegistraMedicoForm(forms.models.ModelForm):

    class Meta:
        model = Medico
        fields = ['nombre', 'ap_paterno', 'ap_materno', 'rfc', 'nacimiento', 'email', 'username']
        labels = {'nombre': 'Nombre', 'ap_paterno': 'Apellido Paterno',
                  'ap_materno': 'Apellido Paterno', 'rfc': 'RFC',
                  'nacimiento' : 'Fecha de Nacimiento', 'email': 'Correo Electronico',
                  }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'rfc': forms.TextInput(attrs={'class': 'form-control'}),
            'nacimiento': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'username': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def save(self):
        return super().save()


class EditaMedicoForm(forms.models.ModelForm):

    class Meta:
        model = Medico
        fields = ['nombre', 'ap_paterno', 'ap_materno', 'rfc', 'nacimiento', 'email', 'username']
        labels = {'nombre': 'Nombre', 'ap_paterno': 'Apellido Paterno',
                  'ap_materno': 'Apellido Paterno', 'rfc': 'RFC',
                  'nacimiento' : 'Fecha de Nacimiento', 'email': 'Correo Electronico',
                  }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'rfc': forms.TextInput(attrs={'class': 'form-control'}),
            'nacimiento': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'username': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def save(self):
        return super().save()
