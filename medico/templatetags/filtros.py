from django.template import Library

register = Library()

@register.filter(name='add_clase')
def add_clase(campo, clase):
	return campo.as_widget(attrs={"class":clase})
