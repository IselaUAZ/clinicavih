from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from django.core.mail import EmailMessage, send_mail, EmailMultiAlternatives
from django.contrib.auth.hashers import make_password
from django.utils.datetime_safe import datetime
from .models import Medico
from paciente.models import Paciente
from .forms import RegistraMedicoForm, EditaMedicoForm
from administrador.models import Administrador
from datetime import timezone
import string
import random
import traceback

template_folder = 'medico/'
template_administrador = 'administrador/'


def inicio(request):
    """función para mostrar el inicio del administrador"""
    return render(request, 'base/base_medico.html', {})


def medico_alta(request):
    chars = string.ascii_uppercase + string.digits
    size = 6
    rest = ''.join(random.choice(chars) for _ in range(size))

    form = RegistraMedicoForm()
    print(request.method)
    if request.method == 'POST':
        form = RegistraMedicoForm(data=request.POST)

        if form.is_valid():
            medico = form.save()
            medico.password = rest
            send_email(medico, rest)
            print(medico.password)
            medico.save()
            medico.password = make_password(rest)
            #medico.set_password(form.cleaned_data['password'])
            medico.save()
            return redirect('/medico/lista')

        template_use = template_folder
        if Administrador.objects.filter(
            username=request.user.username).count() == 1:
            template_use = template_administrador

        form = RegistraMedicoForm()
        return render(request, template_use + 'medico_alta.html',
                      {'form': form})

    return render(
        request,
        'administrador/medico_alta.html',
        {'form': form})


def send_email(medico, rest):
    print (rest)
    message_rest = ''
    message_rest += rest
    subject = 'Bienvenido al Sistema Pre-VIH'
    text_content = 'Tu contraseña y usuario para ingresar al sistema es el siguiente: '
    html_content = '<h2>Tu Usuario y Contraseña para ingresar a </2><p>Sistema Pre-VIH es: <br></p><br> <h2 style="font-family: Andale Mono,AndaleMono,monospace; font-size:16px; font-style:normal;">' + medico.username +' '+ message_rest + '</p></h2>'
    from_email = '"IFA" <iselafraire@outlook.com>'
    msg = EmailMultiAlternatives(subject, text_content, from_email,
                                 [medico.email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
# Create your views here.


def medico_lista(request):
    medicos = Medico.objects.all()
    orderBy = request.POST.get('ordenar')
    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request, template_use + 'medico_lista.html',
                  {'medicos': medicos, 'encabezado': 'Lista de Medicos'})


def medico_modifica(request, pk):
    medico = Medico.objects.get(pk=pk)

    if request.method == 'GET':
        form = EditaMedicoForm(instance=medico)
    else:
        form = EditaMedicoForm(request.POST, instance=medico)
        if form.is_valid():
            form.save()
            return redirect('/medico/lista')

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request,
                  template_use + 'medico_modifica.html', {'form': form})
