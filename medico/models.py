from django.db import models
from django.contrib.auth.models import User
import datetime



# Create your models here.
class Medico(User):
    nombre = models.CharField(u'Nombre', max_length=50)
    ap_paterno = models.CharField(u'Paterno', max_length=30)
    ap_materno = models.CharField(u'Materno', max_length=30, null=True)
    rfc = models.CharField(u'RFC', max_length=10)
    nacimiento = models.DateField(u'Fecha Nacimiento')
    User._meta.get_field('email')._unique = True
    contrasenia = models.CharField(u'Contraseña', max_length=100, null=True)

    def __str__(self):
        return '{}'.format(self.nombre + ' ' + self.ap_paterno +
                               ' ' + self.ap_materno)
