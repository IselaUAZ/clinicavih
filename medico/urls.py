from django.conf.urls import url
from medico import views

urlpatterns = [
    url(r'^inicio$', views.inicio, name='inicio_medico'),
    url(r'^lista', views.medico_lista, name='medico_lista'),
    url(r'^alta', views.medico_alta, name='medico_alta'),
    url(r'modifica/(?P<pk>\d+)/', views.medico_modifica, name='medico_modifica'),
]
