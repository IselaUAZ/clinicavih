from django import forms
from .models import Cita, Paciente, EmbarazoAnterior, EmbarazoActual, Tratamiento
from . models import Suspension, EfectosAdversos, Adherencia, Estadificacion

class AgendaCitaForm(forms.models.ModelForm):

    #Modificar el modelo para obtener el valor de primer cita
    #al igual que los campos default
    class Meta:
        model = Cita
        fields = ['paciente_fk','carga_viral','fecha_cita', 'hora_cita',]
        labels = {'paciente_fk': 'Paciente',
                  'carga_viral': 'Tipo', 
                  'fecha_cita': 'Fecha Cita', 
                  'hora_cita': 'Hora',                  
                  }
        widgets = {
            'paciente_fk': forms.Select(attrs={'class': 'form-control'}),
            'carga_viral': forms.Select(attrs={'class': 'form-control'}),
            'fecha_cita': forms.TextInput(attrs={'class': 'form-control'}),
            'hora_cita': forms.Select(attrs={'class': 'form-control'}),
            
            
        }

    def __init__(self, *args, **kwargs):
        super(AgendaCitaForm, self).__init__(*args, **kwargs)
        self.fields['paciente_fk'].queryset = Paciente.objects.all()
        #self.fields['id_estado_fk'].queryset = Municipio.objects.filter(id_estado_id=32)

    def save(self):
        return super().save()


class AtiendeCitaForm(forms.models.ModelForm):

    class Meta:
        model = Cita
        fields = ['carga_viral',
                  'fecha_cita', 'hora_cita','peso','talla',
                  'presion_art','frec_cardiaca','frec_resp',
                  'temperatura','es_primer_cita','carga_viral',
                  'imc','embarazo_act','embarazo_ant']

        labels = {
                  'carga_viral': 'Tipo', 
                  'fecha_cita': 'Fecha Cita', 
                  'hora_cita': 'Hora',
                  'peso':'Peso',
                  'talla':'Talla',
                  'presion_art':'Presion Arterial',
                  'frec_cardiaca':'Frecuencia Cardiaca',
                  'frec_resp':'Frecuencia Respiratoria',
                  'temperatura':'Temperatura',
                  'es_primer_cita':'Primer Cita',
                  'carga_viral':'Carga Viral',
                  'imc':'Indice Masa Corporal',
                  'embarazo_act':'¿Esta embarazada actualmente?',
                  'embarazo_ant':'¿Ha tenido embarazos anteriormente?',
                  }
        widgets = {
            
            'carga_viral': forms.Select(attrs={'class': 'form-control'}),
            'fecha_cita': forms.TextInput(attrs={'class': 'form-control'}),
            'hora_cita': forms.Select(attrs={'class': 'form-control'}),
            'peso': forms.TextInput(attrs={'class': 'form-control'}),
            'talla': forms.TextInput(attrs={'class': 'form-control'}),
            'presion_art': forms.TextInput(attrs={'class': 'form-control'}),
            'frec_cardiaca': forms.TextInput(attrs={'class': 'form-control'}),
            'frec_resp': forms.TextInput(attrs={'class': 'form-control'}),
            'temperatura': forms.TextInput(attrs={'class': 'form-control'}),
            'es_primer_cita': forms.Select(attrs={'class': 'form-control'}),            
            'imc': forms.TextInput(attrs={'class': 'form-control'}),
            'embarazo_act': forms.Select(attrs={'class': 'form-control'}),
            'embarazo_ant': forms.Select(attrs={'class': 'form-control'}),            
        }

    def __init__(self, *args, **kwargs):
        super(AtiendeCitaForm, self).__init__(*args, **kwargs)
        #self.fields['paciente_fk'].queryset = Paciente.objects.all()
        #self.fields['id_estado_fk'].queryset = Municipio.objects.filter(id_estado_id=32)

    def save(self):
        return super().save()



class EmbarazoActualForm(forms.models.ModelForm):
    class Meta:
        model = EmbarazoActual
        fields = ['semana_gestacion','trat_antiretro','inicio_trat_antiretro','esquemas','intraparto_act','tipo_intra_act','tiempo_intra_act',
        ]
        labels = {
                  'semana_gestacion': 'Semana de Gestacion',                                
                  'trat_antiretro': '¿Emplea tratamiento antiretroviral?',                                
                  'inicio_trat_antiretro': 'Semana en que inicio el tratamiento',                                
                  'esquemas': 'Esquemas empleados',                                
                  'intraparto_act': 'Profilaxis intraparto',                                
                  'tipo_intra_act': 'Tipo',                                
                  'tiempo_intra_act': 'Tiempo',                          
                  }
        widgets = {
            'semana_gestacion': forms.TextInput(attrs={'class': 'form-control'}),            
            'trat_antiretro': forms.Select(attrs={'class': 'form-control'}),
            'inicio_trat_antiretro': forms.TextInput(attrs={'class': 'form-control'}),            
            'esquemas': forms.TextInput(attrs={'class': 'form-control'}),
            'intraparto_act': forms.Select(attrs={'class': 'form-control'}),            
            'tipo_intra_act': forms.TextInput(attrs={'class': 'form-control'}),
            'tiempo_intra_act': forms.TextInput(attrs={'class': 'form-control'}),          
            
        }

    def save(self):
        return super().save()


    
class EmbarazoAntecedenteForm(forms.models.ModelForm):
    class Meta:
        model = EmbarazoAnterior
        fields = ['perdida','nacimiento','intraparto_ant'
                 ,'tipo_intra_ant','tiempo_intra_ant','posparto_ant'
                 ,'tipo_pos_ant','tiempo_pos_ant','metodo_emb','lactancia',
        ]
        labels = {
                  'perdida': '¿Ha tenido perdida de producto?',                                
                  'nacimiento': 'Fecha de nacimiento',                                
                  'intraparto_ant': 'Profilaxis intraparto',                                
                  'tipo_intra_ant': 'Tipo de profiliaxis intraparto',                                
                  'tiempo_intra_ant': 'Tiempo',                                
                  'posparto_ant': 'Profiliaxis posparto',                                
                  'tipo_pos_ant': 'Tipo de profiliaxis posparto',
                  'tiempo_pos_ant':'Tiempo',                          
                  'metodo_emb':'Metodo de resolucion del embarazo',
                  'lactancia':'¿Recibio lactancia?',
                  }
        widgets = {
            'perdida': forms.Select(attrs={'class': 'form-control'}),            
            'nacimiento': forms.TextInput(attrs={'class': 'form-control'}),
            'intraparto_ant': forms.Select(attrs={'class': 'form-control'}),            
            'tipo_intra_ant': forms.TextInput(attrs={'class': 'form-control'}),
            'tiempo_intra_ant': forms.TextInput(attrs={'class': 'form-control'}),            
            'posparto_ant': forms.Select(attrs={'class': 'form-control'}),
            'tipo_pos_ant': forms.TextInput(attrs={'class': 'form-control'}),          
            'tiempo_pos_ant': forms.TextInput(attrs={'class': 'form-control'}),          
            'metodo_emb': forms.Select(attrs={'class': 'form-control'}),
            'lactancia': forms.Select(attrs={'class': 'form-control'}),
        }

    def save(self):
        return super().save()


class TratamientoForm(forms.models.ModelForm):
    class Meta:
        model = Tratamiento
        fields = ['fecha_inicio_trat', 'carga_trat', 'log_trat', 'cdcuatro', 'porc_cdcuatro',
        'cdocho', 'porc_cdocho', 'trat_casual', 'trat_efect', 'tratamiento_antirro', 'indicaciones',
        'farmaco_profilaxis'
        ]

        labels = {
            'fecha_inicio_trat': 'Fecha Inicio de Tratamiento',
            'carga_trat': 'Carga de Tratamiento',
            'log_trat':'Logaritmo de Tratamiento',
            'cdcuatro':'CD4',
            'porc_cdcuatro':'%CD4',
            'cdocho':'CD8',
            'porc_cdocho':'%CD8',
            'trat_casual':'Tratamiento Casual',
            'trat_efect':'Efecto Tratamiento',
            'tratamiento_antirro':'Tratamiento Antirretroviral',
            'indicaciones': 'Indicaciones',
            'farmaco_profilaxis': 'Farmaco Profilaxis'
        }
        widgets = {
            'fecha_inicio_trat': forms.TextInput(attrs={'class': 'form-control'}),
            'carga_trat': forms.TextInput(attrs={'class': 'form-control'}),
            'log_trat': forms.TextInput(attrs={'class': 'form-control'}),
            'cdcuatro': forms.TextInput(attrs={'class': 'form-control'}),
            'porc_cdcuatro': forms.TextInput(attrs={'class': 'form-control'}),
            'cdocho': forms.TextInput(attrs={'class': 'form-control'}),
            'porc_cdocho': forms.TextInput(attrs={'class': 'form-control'}),
            'trat_casual': forms.TextInput(attrs={'class': 'form-control'}),
            'trat_efect': forms.TextInput(attrs={'class': 'form-control'}),
            'tratamiento_antirro': forms.Select(attrs={'class': 'form-control'}),
            'indicaciones': forms.TextInput(attrs={'class': 'form-control'}),
            'farmaco_profilaxis': forms.Select(attrs={'class': 'form-control'}),
        }


    def __init__(self, *args, **kwargs):
        super(TratamientoForm, self).__init__(*args, **kwargs)


    def save(self):
        return super().save()


class VacioForm(forms.models.ModelForm):
    class Meta:
        model = Suspension
        fields = []
        labels = {}
        widgets = {}

    def __init__(self, *args, **kwargs):
        super(VacioForm, self).__init__(*args, **kwargs)



class SuspensionForm(forms.models.ModelForm):
    class Meta:
        model = Suspension
        fields = ['fecha_susp', 'motivo'
        ]
        labels = {'fecha_susp': 'Fecha de Suspensión', 'motivo': 'Motivo de Suspensión'
        }
        widgets = {'fecha_susp': forms.TextInput(attrs={'class': 'form-control'}),
                    'motivo': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(SuspensionForm, self).__init__(*args, **kwargs)
    
    def save(self):
        return super().save()


class EfectosAdversosForm(forms.models.ModelForm):
    class Meta:
        model = EfectosAdversos
        fields = ['tipo_efect', 'fecha_efect']
        labels = {'fecha_efect': 'Fecha efecto adverso', 'tipo_efect':'Tipo Efecto'}
        widgets = {'fecha_efect': forms.TextInput(attrs={'class': 'form-control'}),
                    'tipo_efect': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(EfectosAdversosForm, self).__init__(*args, **kwargs)

    def save(self):
        return super().save()


class EstadificacionForm(forms.models.ModelForm):
    class Meta:
        model = Estadificacion
        fields = ['estado','manifestacion','fecha_est','tratamiento','suspension','motivo_est','terminacion',
        ]
        labels = {
                  'estado': 'Estado',                                
                  'manifestacion': 'Manifestación',                                
                  'fecha_est': 'Fecha Inicio',                                
                  'tratamiento': 'Tratamiento',                                
                  'suspension': 'Suspensión',                                
                  'motivo_est': 'Motivo',                                
                  'terminacion': 'Fecha Terminacion'                          
                  }
        widgets = {
            'estado': forms.Select(attrs={'class': 'form-control'}),            
            'manifestacion': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_est': forms.TextInput(attrs={'class': 'form-control'}),            
            'tratamiento': forms.TextInput(attrs={'class': 'form-control'}),
            'suspension': forms.Select(attrs={'class': 'form-control'}),            
            'motivo_est': forms.TextInput(attrs={'class': 'form-control'}),
            'terminacion': forms.TextInput(attrs={'class': 'form-control'}),          
            
        }

    def save(self):
        return super().save()


class AdherenciaForm(forms.models.ModelForm):
    class Meta:
        model = Adherencia
        fields = ['porc_adh','esquema_adh','mod_adh','esquema_ant_adh',
        ]
        labels = {
                  'porc_adh': 'Pocentaje Adherencia',                                
                  'esquema_adh': 'Esquema empleado',                                
                  'mod_adh': '¿Ha tenido modificacion?',                                
                  'esquema_ant_adh': 'Esquema Anterior'                                                       
                  }
        widgets = {
            'porc_adh': forms.TextInput(attrs={'class': 'form-control'}),            
            'esquema_adh': forms.Select(attrs={'class': 'form-control'}),
            'mod_adh': forms.Select(attrs={'class': 'form-control'}),            
            'esquema_ant_adh': forms.Select(attrs={'class': 'form-control'}),            
        }

    def save(self):
        return super().save()
