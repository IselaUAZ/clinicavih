# Generated by Django 2.1.2 on 2018-11-15 22:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cita', '0003_auto_20181115_1931'),
    ]

    operations = [
        migrations.AddField(
            model_name='adherencia',
            name='esquema_ant_adh',
            field=models.IntegerField(choices=[(1, '1ro'), (2, '2do'), (3, '3ro'), (4, '4to')], default=1),
        ),
        migrations.AlterField(
            model_name='adherencia',
            name='mod_adh',
            field=models.IntegerField(default=0, verbose_name=((0, 'No'), (1, 'Si'))),
        ),
        migrations.AlterField(
            model_name='estadificacion',
            name='suspension',
            field=models.IntegerField(choices=[(0, 'No'), (1, 'Si')], default=0),
        ),
    ]
