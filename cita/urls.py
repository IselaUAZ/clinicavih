from django.conf.urls import url
from cita import views

urlpatterns = [
   url(r'^agendar', views.agenda_cita, name='nueva_cita'),
   url(r'^lista', views.cita_lista, name='cita_lista'),
   url(r'atiende/(?P<pk>\d+)/', views.atiende_cita, name='atender_cita'),
   url(r'^embarazoactual', views.embarazo_act_llenado, name='embarazo_actual'),
   url(r'^embarazoantecedente', views.embarazo_ant_llenado, name='embarazo_antecedente'),
   url(r'^iniciotrat/(?P<pk>\d+)/', views.inicio_tratamiento, name='inicio_tratamiento'),
   url(r'^tratamiento', views.tratamiento, name='tratamiento_cita'),
]
