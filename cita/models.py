from django.db import models
from cita.options import *
from paciente.models import Paciente

# Create your models here.


class Cita(models.Model):
    id_cita = models.AutoField(primary_key=True)
    fecha_cita = models.DateField(null=False)
    hora_cita = models.IntegerField(choices=HORA_CHOICES, default=1)
    peso  = models.DecimalField(max_digits=30, decimal_places=15, null=True)
    talla = models.DecimalField(max_digits=30, decimal_places=15, null=True)
    presion_art = models.DecimalField(max_digits=30, decimal_places=15, null=True)
    frec_cardiaca = models.DecimalField(max_digits=30, decimal_places=15, null=True)
    frec_resp = models.DecimalField(max_digits=30, decimal_places=15,null=True)
    temperatura = models.IntegerField(null=True)
    es_primer_cita = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    carga_viral = models.IntegerField(choices=TIPO_CHOICES,default=0)
    imc = models.DecimalField(max_digits=30, decimal_places=15,null=True)
    embarazo_act = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    embarazo_ant = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    paciente_fk = models.ForeignKey(Paciente, null=True,
                                         on_delete=models.CASCADE)


class EmbarazoAnterior(models.Model):
    id_embarazo_ant = models.AutoField(primary_key=True)    
    perdida = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    nacimiento = models.DateField(null=False)  
    intraparto_ant = models.IntegerField(choices=COMMUN_CHOICES, default=1)
    tipo_intra_ant=models.CharField(max_length=45,default=1)
    tiempo_intra_ant = models.IntegerField(default=1)
    posparto_ant = models.IntegerField(choices=COMMUN_CHOICES, default=1)
    tipo_pos_ant=models.CharField(max_length=45,default=1)
    tiempo_pos_ant = models.IntegerField(default=1)
    metodo_emb = models.IntegerField(choices=METODO_EMB_CHOICES, default=0)
    lactancia = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    cita_fk = models.ForeignKey(Cita, null=True,
                                         on_delete=models.CASCADE)


class EmbarazoActual(models.Model):
    id_embarazo_act = models.AutoField(primary_key=True)
    semana_gestacion = models.IntegerField(default=1)    
    trat_antiretro=models.IntegerField(choices=TRAT_ANTIRRETROVIRAL_CHOICES,default=24)
    inicio_trat_antiretro=models.IntegerField(default=1)
    esquemas = models.CharField(max_length=45)
    intraparto_act = models.IntegerField(choices=COMMUN_CHOICES, default=1)
    tipo_intra_act=models.CharField(max_length=45,default=1)
    tiempo_intra_act = models.IntegerField(default=1)
    cita_fk = models.ForeignKey(Cita, null=True,
                                        on_delete=models.CASCADE)


class EmbarazoProfilaxis(models.Model):
    id_profilaxis = models.AutoField(primary_key=True)
    tipo_etapa_emb = models.IntegerField(choices=TIPO_EMB_CHOICES, default=1)
    tiempo = models.IntegerField(default=1)
    tipo = models.CharField(max_length=45)
    emb_act_fk = models.ForeignKey(EmbarazoActual, null=True,
                                                    on_delete=models.CASCADE)


class Adherencia(models.Model):
    id_adherencia = models.AutoField(primary_key=True)
    porc_adh = models.DecimalField(max_digits=30, decimal_places=15)
    esquema_adh = models.IntegerField(choices=ESQUEMA_CHOICES, default=1)
    mod_adh = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    esquema_ant_adh = models.IntegerField(choices=ESQUEMA_CHOICES, default=1)
    cita_fk = models.ForeignKey(Cita, null=True,
                                        on_delete=models.CASCADE)


class Estadificacion(models.Model):
    id_estadificacion = models.AutoField(primary_key=True)
    estado = models.IntegerField(choices=ESTADO_CHOICES, default=1)
    manifestacion = models.CharField(max_length=45)
    fecha_est = models.DateField(null=False)
    tratamiento = models.CharField(max_length=100)
    suspension = models.IntegerField(choices=COMMUN_CHOICES,default=0)
    motivo_est = models.CharField(max_length=100)
    terminacion = models.DateField(null=False)
    cita_fk = models.ForeignKey(Cita, null=True,
                                        on_delete=models.CASCADE)




class Etapa(models.Model):
    id_etapa = models.AutoField(primary_key=True)
    etapa = models.IntegerField(choices=ETAPA_CHOICES, default=1)
    fecha_inicio = models.DateField(null=False)
    fecha_final = models.DateField(null=False)



class Farmaco(models.Model):
    id_farmaco = models.AutoField(primary_key=True)
    nombre_farmaco = models.IntegerField(choices=FARMACO_CHOICES ,default=1)


class EtapaFarmaco(models.Model):
    etapa_fk = models.ForeignKey(Etapa, null=True,
                                            on_delete=models.CASCADE)
    farmaco_fk = models.ForeignKey(Farmaco, null=True,
                                                on_delete=models.CASCADE)


class Antifimico(models.Model):
    id_antifimico = models.AutoField(primary_key=True)
    fecha_diag_anti = models.DateField(null=False)
    localización = models.CharField(max_length=50)
    met_diag = models.IntegerField(choices=METODO_DIAG_CHOICES, default=1)
    tuber = models.IntegerField(default=1)
    curado = models.IntegerField(default=1)


class TratamientoAntifimico(models.Model):
    id_tratam_anti = models.AutoField(primary_key=True)
    trat_virgen = models.CharField(max_length=50)
    recaida = models.IntegerField()
    trat_rec = models.CharField(max_length=50)
    fecha_rec = models.DateField(null=False)
    trat_resist = models.CharField(max_length=50)
    antifimico_fk = models.ForeignKey(Antifimico, null=True,
                                                    on_delete=models.CASCADE)


class TipoTratamiento(models.Model):
    id_tipo_tratamiento = models.AutoField(primary_key=True)
    etapa_fk = models.ForeignKey(Etapa, null=True,
                                            on_delete=models.CASCADE)
    antifimico_fk = models.ForeignKey(Antifimico, null=True,
                                                    on_delete=models.CASCADE)

class Tratamiento(models.Model):
    id_tratamiento = models.AutoField(primary_key=True)
    fecha_inicio_trat = models.DateField(null=False)
    carga_trat = models.IntegerField()
    log_trat = models.IntegerField()
    cdcuatro = models.IntegerField()
    porc_cdcuatro = models.DecimalField(max_digits=30, decimal_places=15)
    cdocho = models.IntegerField()
    porc_cdocho = models.DecimalField(max_digits=30, decimal_places=15)
    trat_casual = models.CharField(max_length=50)
    trat_efect = models.CharField(max_length=50)
    tratamiento_antirro = models.IntegerField(choices=TRAT_ANTIRRETROVIRAL_CHOICES, default=1)
    indicaciones = models.CharField(max_length=100)
    farmaco_profilaxis = models.IntegerField(choices=PROFILAXIS_CHOICES, default=1)
    cita_fk = models.ForeignKey(Cita, null=True,
                                        on_delete=models.CASCADE)
    tipo_tratamiento_fk = models.ForeignKey(TipoTratamiento, null=True,
                                                                on_delete=models.CASCADE)


class EfectosAdversos(models.Model):
    id_efectos_adv = models.AutoField(primary_key=True)
    tipo_efect = models.CharField(max_length=50)
    fecha_efect = models.DateField(null=False)
    tratamiento_fk = models.ForeignKey(Tratamiento, null=True,
                                                        on_delete=models.CASCADE)


class Suspension(models.Model):
    id_suspension = models.AutoField(primary_key=True)
    fecha_susp = models.DateField(null=False)
    motivo = models.CharField(max_length=100)
    tratamiento_fk = models.ForeignKey(Tratamiento, null=True,
                                                        on_delete=models.CASCADE)

