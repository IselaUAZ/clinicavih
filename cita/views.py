from django.shortcuts import render
from django.shortcuts import redirect
from .models import Paciente
from .forms import *
from administrador.models import Administrador
from django.utils.datetime_safe import datetime
from datetime import timezone
import string
import random
import traceback



# Create your views here.
template_folder = 'cita/'
template_administrador = 'administrador/'


def agenda_cita(request):    
    form = AgendaCitaForm()
    print(request.method)
    
    if request.method == 'POST':
        form = AgendaCitaForm(data=request.POST)
        if form.is_valid():
            medico = form.save()
            medico.save()
            return redirect('/cita/lista')

        
        if Administrador.objects.filter(
            username=request.user.username).count() == 1:
            template_use = template_administrador

        form = AgendaCitaForm()
        return render(request, 'cita/agenda_cita.html',
                      {'form': form})

    return render(
        request,
        'cita/agenda_cita.html',
        {'form': form})


def cita_lista(request):
    citas = Cita.objects.all()
    orderBy = request.POST.get('ordenar')
    print (citas)
    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request, template_use + 'cita_lista.html',
                  {'citas': citas, 'encabezado': 'Citas'})


def atiende_cita(request, pk):
    cita = Cita.objects.get(pk=pk)
    #OBTIENE PACIENTE
    id = cita.paciente_fk.id_paciente
    paciente = Paciente.objects.get(id_paciente=id)

    formAd = AdherenciaForm()
    formEst = EstadificacionForm() 

    if request.method == 'GET':
        ##AQUI FALTA OBTENER LA ADHERENCIA Y ESTADIFICACION
        form = AtiendeCitaForm(instance=cita)
    else:
        form = AtiendeCitaForm(request.POST, instance=cita)
        formAd = AdherenciaForm(request.POST)
        formEst = EstadificacionForm(request.POST)

        if form.is_valid() and formAd.is_valid and formEst.is_valid:
            form.save()

            form_ad_saved = formAd.save()
            form_ad_saved.cita_fk = Cita.objects.get(id_cita=pk)
            form_ad_saved.save()

            form_est_saved = formEst.save()
            form_est_saved.cita_fk = Cita.objects.get(id_cita=pk)
            form_est_saved.save()


            #SE VERIFICA SI ES PRIMER CITA Y EL SEXO DEL PACIENTE
            if paciente.sexo == 1 :
                #significa que es la primer cita 
                if cita.es_primer_cita == 1 & cita.embarazo_ant==1:
                    return redirect('/cita/embarazoantecedente&id_cita=' + pk)
                else:
                    if cita.embarazo_act == 1:
                        return redirect('/cita/embarazoactual&id_cita=' + pk)
                    else:
                        return redirect('/cita/lista')

            else:
                return redirect('/cita/lista')

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador
    #siempre usa fuckin administrador -.-
    return render(request,
                  template_use + 'atiende_cita.html', {'form': form,'formAd':formAd,'formEst':formEst})


def embarazo_act_llenado(request):
    id_cita = request.build_absolute_uri().split('%3D')[1]
    print('-'+id_cita+'-')
    print(Cita.objects.get(id_cita=id_cita))
    print(request.GET.keys())

    if request.method == 'GET':
        form = EmbarazoActualForm()
    else:
        form = EmbarazoActualForm(request.POST)
        if form.is_valid():
            form_saved = form.save()

            form_saved.cita_fk = Cita.objects.get(id_cita=id_cita)
            form_saved.save()
            return redirect('/cita/lista')

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador
    return render(request,
                  template_use + 'embarazo_act.html', {'form': form})


def inicio_tratamiento(request, pk):
    cita = Cita.objects.get(pk=pk)
    
    if request.method == 'GET':
        form = VacioForm(instance=cita)
    else: 
        form = VacioForm(request.POST, instance=cita)
        if form.is_valid():
            #form.save()
            return redirect('/cita/tratamiento&id_cita=' + pk)
    
    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request,
                template_use + 'tratamiento_cita.html', {'form': form})


def tratamiento(request):
    id_cita = request.build_absolute_uri().split('%3D')[1]

    if request.method == 'GET':
        form = TratamientoForm()
        form_2 = SuspensionForm()
        form_3 = EfectosAdversosForm()
    else:
        form = TratamientoForm(request.POST)
        form_2 = SuspensionForm(request.POST)
        form_3 = EfectosAdversosForm(request.POST)
        if form.is_valid() and form_2.is_valid() and form_3.is_valid():
            trat = form.save()
            trat.cita_fk = Cita.objects.get(id_cita=id_cita)
            trat.save()
            trat.cita_fk = Cita.objects.get(id_cita=id_cita)
            trat.save()
            trat.cita_fk = Cita.objects.get(id_cita=id_cita)
            trat.save()
            return redirect('/cita/lista')
    
    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador
    return render(request,
                  template_use + 'tratamiento_cita.html', {'form': form, 'form_2':form_2, 'form_3':form_3})

def embarazo_ant_llenado(request):
    id_cita = request.build_absolute_uri().split('%3D')[1]
    print('-'+id_cita+'-')
    print(Cita.objects.get(id_cita=id_cita))
    print(request.GET.keys())

    

    if request.method == 'GET':
        form = EmbarazoAntecedenteForm()
    else:
        form = EmbarazoAntecedenteForm(request.POST)
        print(form)
        if form.is_valid():
            form_saved = form.save()            
            form_saved.cita_fk = Cita.objects.get(id_cita=id_cita)
            form_saved.save()
            
            print('ID CITAAAAAAAAAA')
            print(form_saved.cita_fk.id_cita)
            
            cita = Cita.objects.get(id_cita=id_cita)

            if cita.embarazo_act==1:          
                return redirect('/cita/embarazoactual&id_cita=' + id_cita)
            else: 
                return redirect('/cita/lista')


    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador
    return render(request,
                  template_use + 'embarazo_ant.html', {'form': form})
