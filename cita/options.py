COMMUN_CHOICES = (
    (0, ("No")),
    (1, ("Si")),    
)

HORA_CHOICES = (
    (8, ("8:00 AM")),
    (9, ("9:00 AM")),
    (10, ("10:00 AM")),
    (11, ("11:00 AM")),
    (12, ("12:00 PM")),
    (13, ("13:00 PM")),
    (14, ("14:00 PM")),
    (15, ("15:00 PM")),
)

TIPO_CHOICES = (
    (0, ("Mensual")),
    (1, ("Carga Viral")),
)

TIPO_EMB_CHOICES = (
    (1, ("Profilaxis Intraparto")),
    (2, ("Profilaxis Posparto"))
)

ESQUEMA_CHOICES = (
    (1, ("1ro")),
    (2, ("2do")),
    (3, ("3ro")),
    (4, ("4to"))
)

ESTADO_CHOICES = (
    (1, ("A")),
    (2, ("B")),
    (3, ("C"))
)



METODO_DIAG_CHOICES = (
    (1, ("Baciloscopía")),
    (2, ("Cultivo")),
    (3, ("Histopatología")),
    (4, ("Cuadro Clínico")),
    (5, ("Radiológico")),
    (6, ("Epidemiológico")),
    (7, ("TAC")),
    (8, ("PCR")),
    (9, ("Citoquímico")),
    (10, ("Otro"))
)

ETAPA_CHOICES = (
    (1, ("1 (durante dos meses)")),
    (2, ("2 (durante cuatro meses)"))
)

FARMACO_CHOICES = (
    (1, ("Rifampicina")),
    (2, ("Pirazinamida")),
    (3, ("Etanbutol")),
    (4, ("Isoniazida")),
    (5, ("Otro"))
)


TRAT_ANTIRRETROVIRAL_CHOICES = (
    (1, ("Abacavir")),
    (2, ("Atazanavir")),
    (3, ("Atripla")),
    (4, ("Cobicistat")),
    (5, ("Combivir (lamivudina zidovudina)")),
    (6, ("Daronavir")),
    (7, ("Didanosina")),
    (8, ("Dolutegravir")),
    (9, ("Efavirenz")),
    (10, ("Emtricitabina")),
    (11, ("Enfuvirtida")),
    (12, ("Etravirina")),
    (13, ("Lopinavir")),
    (14, ("Ritonavir")),
    (15, ("Nevirapina")),
    (16, ("Raltegravir")),
    (17, ("Rilpivirina")),
    (18, ("Ritonavir")),
    (19, ("Tipranavir")),
    (20, ("Truvada")),
    (21, ("Caletra")),
    (22, ("Trubada")),
    (23, ("Otro")),
    (24, ("Ninguno"))
)

PROFILAXIS_CHOICES = (
    (1, ("Trimetroprim")),
    (2, ("Itraconazol")),
    (3, ("Fluconazol")),
    (4, ("Azitromicina")),
    (5, ("Otro"))
)

METODO_EMB_CHOICES = (
    (1, ("Natural")),
    (2, ("Cesaría"))
)
