from django.conf.urls import url
from paciente import views

urlpatterns = [
    url(r'^alta', views.paciente_alta, name='paciente_alta'),
    url(r'^modifica/(?P<pk>\d+)/', views.paciente_modifica, name='paciente_modifica'),
    url(r'^lista', views.paciente_lista, name='paciente_lista'),
    url(r'^crearant/(?P<pk>\d+)/', views.crear_antecedente, name='crear_antecedente'),
    url(r'^antlista', views.antecedente_lista, name='antecedente_lista'),
    # url(r'antecedentepaciente', views.antecedentes_paciente, name='antecedentes_paciente'),
    # url(r'^crearpato/(?P<pk>\d+)/', views.crear_antecedente_pato, name='crear_antecedente_pato'),
    url(r'^antecedentepatologico', views.antecedentes_patologicos, name='antecedentes_patologicos'),
    
]
