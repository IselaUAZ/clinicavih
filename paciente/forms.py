from django import forms
from .views import Paciente
from .models import Paciente, Municipio, Estado, AntecedentesPaciente, Telefono, Transfusion,Medico
from .models import Hospitalizacion, AntecedentesPatologicos, AntecedentesHeredofamiliares


##############################################################
class RegistraPacienteForm(forms.models.ModelForm):

    class Meta:
        model = Paciente
        fields = ['id_medico_fk', 'nombre_paciente', 'ap_paterno', 'ap_materno', 'edad', 'nacimiento', 'nacionalidad', 'est_civil' ,
        'der_hab', 'institucion', 'folio_seguro','id_estado_fk', 'id_municipio_fk', 'ocupacion', 'domicilio',  'sexo']
        labels = {'id_medico_fk': 'Medico', 'nombre_paciente': 'Nombre', 'ap_paterno': 'Apellido Paterno',
                  'ap_materno': 'Apellido Materno', 'edad': 'Edad',
                  'nacimiento' : 'Fecha de Nacimiento', 'nacionalidad':'Nacionalidad','est_civil': 'Estado Civil','der_hab': 'Derechohabiencia',
                  'institucion': 'Institucion', 'folio_seguro':'Folio Seguro','id_estado_fk': 'Estado','id_municipio_fk': 'Municipio','ocupacion': 'Ocupacion',
                  'domicilio': 'Domicilio','sexo': 'Sexo'
                  }
        widgets = {
            'id_medico_fk': forms.Select(attrs={'class': 'form-control'}),
            'nombre_paciente': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'edad': forms.NumberInput(attrs={'class': 'form-control'}),
            'nacimiento': forms.TextInput(attrs={'class': 'form-control'}),
            'nacionalidad': forms.TextInput(attrs={'class': 'form-control'}),
            'est_civil': forms.Select(attrs={'class': 'form-control'}),
            'der_hab': forms.Select(attrs={'class': 'form-control'}),
            'institucion': forms.Select(attrs={'class': 'form-control'}),
            'folio_seguro': forms.TextInput(attrs={'class': 'form-control'}),
            'id_municipio_fk': forms.Select(attrs={'class': 'form-control'}),
            'id_estado_fk': forms.Select(attrs={'class': 'form-control'}),
            'ocupacion': forms.TextInput(attrs={'class': 'form-control'}),
            'domicilio': forms.TextInput(attrs={'class': 'form-control'}),
            'sexo': forms.Select(attrs={'class': 'form-control'}),
            

        }

    def __init__(self, *args, **kwargs):
        super(RegistraPacienteForm, self).__init__(*args, **kwargs)
        self.fields['id_municipio_fk'].queryset = Municipio.objects.all()
        self.fields['id_estado_fk'].queryset = Estado.objects.all()
        self.fields['id_medico_fk'].queryset = Medico.objects.all()
        

    def save(self):
        return super().save()


class TelefonoForm(forms.models.ModelForm):
    class Meta:
        model = Telefono
        fields = ['tipo', 'numero']
        labels = {'tipo': 'Tipo de Teléfono', 'numero': 'Número de Teléfono'}
        widgets = {
            'tipo_tel': forms.Select(attrs={'class': 'form-control'}),
            'numero': forms.TextInput(attrs={'class': 'form-control'}),}

    def __init__(self, *args, **kwargs):
        super(TelefonoForm, self).__init__(*args, **kwargs)
        #self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()


class TransfusionForm(forms.models.ModelForm):
    class Meta:
        model = Transfusion
        fields = ['fecha_tran', 'motivo', 'reaccion']
        labels = {'fecha_tran': 'Fecha de Transfusion', 'reaccion': 'Reacción adversa'}
        widgets = {
            'fecha_tran': forms.TextInput(attrs={'class': 'form-control'}),
            'motivo': forms.TextInput(attrs={'class': 'form-control'}),
            'reaccion': forms.TextInput(attrs={'class': 'form-control'}),}

    def __init__(self, *args, **kwargs):
        super(TransfusionForm, self).__init__(*args, **kwargs)
        #self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()

##############################################################

class ModificaPacienteForm(forms.models.ModelForm):

    class Meta:
        model = Paciente
        fields = ['nombre_paciente', 'ap_paterno', 'ap_materno', 'edad', 'nacimiento', 'der_hab', 'institucion'
        , 'ocupacion', 'domicilio', 'est_civil', 'sexo','id_estado_fk', 'id_municipio_fk','id_medico_fk',]
        labels = {'id_medico_fk': 'Medico','nombre_paciente': 'Nombre', 'ap_paterno': 'Apellido Paterno',
                  'ap_materno': 'Apellido Materno', 'edad': 'Edad',
                  'nacimiento' : 'Fecha de Nacimiento', 'der_hab': 'Derechohabiencia',
                  'institucion': 'Institucion', 'ocupacion': 'Ocupacion','domicilio': 'Domicilio',
                  'est_civil': 'Estado Civil','sexo': 'Sexo','id_estado_fk': 'Estado','id_municipio_fk': 'Municipio',
                  }
        widgets = {
            'nombre_paciente': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'edad': forms.NumberInput(attrs={'class': 'form-control'}),
            'nacimiento': forms.TextInput(attrs={'class': 'form-control'}),
            'der_hab': forms.Select(attrs={'class': 'form-control'}),
            'institucion': forms.Select(attrs={'class': 'form-control'}),
            'ocupacion': forms.TextInput(attrs={'class': 'form-control'}),
            'domicilio': forms.TextInput(attrs={'class': 'form-control'}),
            'est_civil': forms.Select(attrs={'class': 'form-control'}),
            'sexo': forms.Select(attrs={'class': 'form-control'}),
            'id_municipio_fk': forms.Select(attrs={'class': 'form-control'}),
            'id_medico_fk': forms.Select(attrs={'class': 'form-control'}),
            'id_estado_fk': forms.Select(attrs={'class': 'form-control'}),

        }

    def __init__(self, *args, **kwargs):
        super(ModificaPacienteForm, self).__init__(*args, **kwargs)
        self.fields['id_municipio_fk'].queryset = Municipio.objects.all()
        self.fields['id_estado_fk'].queryset = Estado.objects.all()
        self.fields['id_medico_fk'].queryset = Medico.objects.all()

    def save(self):
        return super().save()

class ModificaTelefonoForm(forms.models.ModelForm):
    class Meta:
        model = Telefono
        fields = ['tipo', 'numero']
        labels = {'tipo': 'Tipo de Teléfono', 'numero': 'Número de Teléfono'}
        widgets = {
            'tipo_tel': forms.Select(attrs={'class': 'form-control'}),
            'numero': forms.TextInput(attrs={'class': 'form-control'}),}

    def __init__(self, *args, **kwargs):
        super(ModificaTelefonoForm, self).__init__(*args, **kwargs)
        #self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()

class ModificaTransfusionForm(forms.models.ModelForm):
    class Meta:
        model = Transfusion
        fields = ['fecha_tran', 'motivo', 'reaccion']
        labels = {'fecha_tran': 'Fecha de Transfusion', 'reaccion': 'Reacción adversa'}
        widgets = {
            'fecha_tran': forms.TextInput(attrs={'class': 'form-control'}),
            'motivo': forms.TextInput(attrs={'class': 'form-control'}),
            'reaccion': forms.TextInput(attrs={'class': 'form-control'}),}

    def __init__(self, *args, **kwargs):
        super(ModificaTransfusionForm, self).__init__(*args, **kwargs)
        #self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()

##############################################################

class AsignarAntForm(forms.models.ModelForm):
    class Meta: 
        model = Paciente
        fields = ['nombre_paciente', 'ap_paterno', 'ap_materno']
        labels = {'nombre_paciente': 'Nombre', 'ap_paterno': 'Apellido Paterno',
                  'ap_materno': 'Apellido Materno'}
        widgets = {'nombre_paciente': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'ap_materno': forms.TextInput(attrs={'class': 'form-control'}),}
    
    def __init__(self, *args, **kwargs):
        super(AsignarAntForm, self).__init__(*args, **kwargs)
        #self.fields['paciente_fk'].queryset = Paciente.objects.all()
        #self.fields['id_estado_fk'].queryset = Municipio.objects.filter(id_estado_id=32)

    def save(self):
        return super().save()


#############################################################

class AntecedentesPacienteForm(forms.models.ModelForm):
    class Meta:
        model = AntecedentesPaciente
        fields = ['factor_riesgo', 'inicio_sexual', 'num_parejas', 'anticonceptivo' , 'fecha_diag_vih', 'metodo_diag', 
        'edad_prob', 'inicio_vih' , 'relacion_hetero', 'relacion_homo', 'hist_inmunizacion']
        labels = {'factor_riesgo': 'Factor de Riesgo', 'inicio_sexual': 'Inicio Sexual:', 'num_parejas': 'Número de Parejas',
         'anticonceptivo': 'Metodo Anticonceptivo', 'fecha_diag_vih': 'Fecha de Diagnositco VIH' , 'metodo_diag': 'Método Diagnóstico VIH', 
         'edad_prob':'Año Probable de Infeccion', 'inicio_vih': 'Inicio de Manifestación VIH', 'relacion_hetero': 'Relaciones Heterosexuales',
         'relacion_homo':' Relaciones Homosexuales', 'hist_inmunizacion': 'Historial Inmunización:', }
        widgets = {
            'factor_riesgo': forms.Select(attrs={'class': 'form-control'}),
            'inicio_sexual': forms.TextInput(attrs={'class': 'form-control'}),
            'num_parejas': forms.TextInput(attrs={'class': 'form-control'}),
            'anticonceptivo': forms.Select(attrs={'class': 'form-control'}),
            'fecha_diag_vih' : forms.TextInput(attrs={'class': 'form-control'}),
            'metodo_diag': forms.Select(attrs={'class': 'form-control'}),
            'edad_prob': forms.TextInput(attrs={'class': 'form-control'}),
            'inicio_vih': forms.TextInput(attrs={'class': 'form-control'}),
            'relacion_hetero': forms.Select(attrs={'class': 'form-control'}),
            'relacion_homo': forms.Select(attrs={'class': 'form-control'}),
            'hist_inmunizacion': forms.Select(attrs={'class': 'form-control'}),
        }

    #def __init__(self, *args, **kwargs):
    #    super(AntecedentesPacienteForm, self).__init__(*args, **kwargs)
    #    self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()

class HeredofamiliaresForm(forms.models.ModelForm):
    class Meta:
        model = AntecedentesHeredofamiliares
        fields = ['nombre_enfermedad', 'familiar_enfermedad']
        labels = {'nombre_enfermedad':'Nombre Enfermedad', 'familiar_enfermedad':'Familiar con esa enfermedad'}
        widgets = {
            'nombre_enfermedad': forms.Select(attrs={'class': 'form-control'}),
            'familiar_enfermedad': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(HeredofamiliaresForm, self).__init__(*args, **kwargs)
    #
    #     self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()

#############################################################

class AntecedentesPatologicoForm(forms.models.ModelForm):
    class Meta:
        model = AntecedentesPatologicos
        fields = ['enfermedad_prev', 'fecha_diag', 'tratamiento', 'tipo_ciru'
        ,'lugar_fract', 'tipo_alg', 'nombre_med_vih']
        labels = {'enfermedad_prev': 'Enfermedad Previa:', 'fecha_diag': 'Fecha de Diagnóstico',
        'tratamiento':'Tratamiento', 'tipo_ciru': 'Tipo de Cirugia', 'lugar_fract': 'Lugar de Fractura',
        'tipo_alg': 'Tipo de Alergia', 'nombre_med_vih': 'Nombre Medicamento VIH:'}
        widgets = {
            'enfermedad_prev': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_diag': forms.TextInput(attrs={'class': 'form-control'}),
            'tratamiento': forms.TextInput(attrs={'class': 'form-control'}),
            'tipo_ciru': forms.TextInput(attrs={'class': 'form-control'}),
            'lugar_fract': forms.TextInput(attrs={'class': 'form-control'}),
            'tipo_alg': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_med_vih': forms.TextInput(attrs={'class': 'form-control'}),
        }

    #def __init__(self, *args, **kwargs):
    #    super(AntecedentesPacienteForm, self).__init__(*args, **kwargs)
    #    self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()


class HospitalizacionForm(forms.models.ModelForm):
    class Meta:
        model = Hospitalizacion
        fields = ['fecha_hosp', 'diagn_hosp', 'tratamiento', 'duracion'
        ,'nombre_hosp']
        labels = {'fecha_hosp': 'Fecha de Hospitalizacion:', 'diagn_hosp': 'Diagnóstico',
        'tratamiento':'Tratamiento', 'duracion': 'Duración de Internado'}
        widgets = {
            'fecha_hosp': forms.TextInput(attrs={'class': 'form-control'}),
            'diagn_hosp': forms.TextInput(attrs={'class': 'form-control'}),
            'tratamiento': forms.TextInput(attrs={'class': 'form-control'}),
            'duracion': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_hosp': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(HospitalizacionForm, self).__init__(*args, **kwargs)
    #    self.fields['id_paciente_fk'].queryset = Paciente.objects.all()

    def save(self):
        return super().save()


class VacioForm(forms.models.ModelForm):
    class Meta:
        model = Telefono
        fields = []
        labels = {}
        widgets = {}

    def __init__(self, *args, **kwargs):
        super(VacioForm, self).__init__(*args, **kwargs)