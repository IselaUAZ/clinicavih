from django.db import models
from django.contrib.auth.models import User
from medico.models import Medico
from paciente.choices import *
import datetime

# Create your models here.
class Estado(models.Model):
    id_estado = models.AutoField(primary_key=True)
    clave = models.CharField(max_length=2)
    nombre = models.CharField(max_length=45)
    abrev = models.CharField(max_length=16)

    def __str__(self):
        return '{}'.format(self.nombre
        )


class Municipio(models.Model):
    id_municipio = models.AutoField(primary_key=True)
    id_estado = models.ForeignKey(Estado, on_delete=models.CASCADE,)
    clave = models.CharField(max_length=3, null=False)
    nombre = models.CharField(max_length=50, null=False)

    def __str__(self):
        return '{}'.format(self.nombre
        )

class Paciente(models.Model):
    id_paciente = models.AutoField(primary_key=True)
    id_medico_fk = models.ForeignKey(Medico, on_delete=models.CASCADE, null=True)
    nombre_paciente = models.CharField(u'Nombre', max_length=50, null=False)
    ap_paterno = models.CharField(u'Paterno', null=False, max_length=30)
    ap_materno = models.CharField(u'Materno', max_length=30, null=True)
    edad = models.IntegerField(u'Edad', null=False)
    nacimiento = models.DateField(u'Fecha Nacimiento', null=False)
    nacionalidad = models.CharField(u'Nacionalidad', max_length=50)
    est_civil = models.IntegerField(u'Estado civil',choices=ESTADO_CIVIL_CHOICES, default=1, null=False)
    der_hab = models.IntegerField(u'Derecho Habiencia', choices=COMMUN_CHOICES)
    institucion = models.IntegerField(u'institucion', choices=INSTITUCION_CHOICES, default=3)
    folio_seguro = models.IntegerField(u'Folio de Seguro')
    id_municipio_fk = models.ForeignKey(Municipio, on_delete=models.CASCADE, null=True)
    id_estado_fk = models.ForeignKey(Estado, on_delete=models.CASCADE, null=True)
    ocupacion = models.CharField(u'Ocupación', max_length=30)
    domicilio = models.CharField(u'Domicilio', null=False, max_length=150)
    sexo = models.IntegerField(u'Sexo', null=False, choices=SEXO_CHOICES, default=1)
    
    

    def __str__(self):
        return '{}'.format(self.nombre_paciente + ' ' + self.ap_paterno +
                               ' ' + self.ap_materno)

class Transfusion(models.Model):
    id_transfusion = models.AutoField(primary_key=True)
    fecha_tran = models.DateField(u'Fecha de Transfusion')
    motivo = models.CharField(u'Motivo', max_length=150)
    reaccion = models.CharField(u'Reaccion', max_length=50)
    id_paciente_fk = models.ForeignKey(Paciente, null=True, on_delete=models.CASCADE)

class Telefono(models.Model):
    id_telefono = models.AutoField(primary_key=True)
    numero = models.CharField(u'Numero', max_length=10)
    tipo = models.IntegerField(u'Tipo', choices=TELEFONO_CHOICES, default=1)
    id_paciente_fk = models.ForeignKey(Paciente, null=True, on_delete=models.CASCADE)

class AntecedentesPaciente(models.Model):
    id_antecedentes = models.AutoField(primary_key=True)
    factor_riesgo = models.IntegerField(u'Factor Riesgo', choices=FACTOR_RIESGO_CHOICES, default=1)
    inicio_sexual = models.IntegerField(u'Inicio Sexual (Edad)')
    num_parejas = models.IntegerField(u'Numero de parejas')
    anticonceptivo = models.IntegerField(u'Anticonceptivo', choices=ANTICONCEPTIVO_CHOICES, default=1)
    fecha_diag_vih = models.IntegerField(u'Fecha de Diagnositco VIH')
    metodo_diag = models.IntegerField(u'Metodo Diagnostico', choices=METODO_DIAG_CHOICES, default=1)
    edad_prob = models.IntegerField(u'Fecha Prob. de Infeccion')
    inicio_vih = models.IntegerField(u'Inicio de manifestacion - VIH')
    relacion_hetero = models.IntegerField(u'Relaciones Heterosexuales', choices=DECISION_CHOICES, default=1)
    relacion_homo = models.IntegerField(u'Relaciones Homosexuales', choices=DECISION_CHOICES ,default=1)
    hist_inmunizacion = models.IntegerField(u'Historial Inmunizacion', choices=HIST_INMUNE_CHOICES, default=1)
    id_paciente_fk = models.ForeignKey(Paciente, on_delete=models.CASCADE, null=True)


class AntecedentesPatologicos(models.Model):
    id_antecedentes_pato = models.AutoField(primary_key=True)
    enfermedad_prev = models.CharField(u'Enfermedad Previa', null=False, max_length=100)
    fecha_diag = models.DateField(u'Fecha Diagnostico', null=False)
    tratamiento = models.CharField(u'Tratamiento', max_length=100)
    tipo_ciru = models.CharField(u'Tipo Cirugia', max_length=50)
    lugar_fract = models.CharField(u'Lugar Fractura', max_length=50)
    tipo_alg = models.CharField(u'Tipo Alergia', max_length=50)
    nombre_med_vih = models.CharField(u' Nombre Medico VIH', max_length=100)
    id_antecedentes_fk = models.ForeignKey(AntecedentesPaciente, null=True, on_delete=models.CASCADE)


class AntecedentesHeredofamiliares(models.Model):
    id_ant_heredofamiliares = models.AutoField(primary_key=True)
    nombre_enfermedad = models.IntegerField(u'Nombre Enfermdad', choices=HEREDOFAMILIARES_CHOICES, default=1)
    familiar_enfermedad = models.CharField(u'Familiar Enfermedad', max_length=50)
    id_antecedentes_fk = models.ForeignKey(AntecedentesPaciente, null=True, on_delete=models.CASCADE)



class Hospitalizacion(models.Model):
    id_hospitalizacion = models.AutoField(primary_key=True)
    fecha_hosp = models.DateField(u'Fecha Hospitalizacion', null=False)
    diagn_hosp = models.CharField(u'Diagnostico', null=False, max_length=50)
    tratamiento = models.CharField(u'Tratamiento', null=False, max_length=50)
    duracion = models.IntegerField(u'Dias')
    nombre_hosp = models.CharField(u'Nombre Hospital', null=False, max_length=50)
    id_antecedentes_pato_fk = models.ForeignKey(AntecedentesPatologicos, null=True ,on_delete=models.CASCADE)

#aqui mero
class AntecedentesPersonales(models.Model):
    id_antecedentes_personales = models.AutoField(primary_key=True)
    escolaridad = models.IntegerField(choices=ESCOLARIDAD_CHOICES, default=1)
    estado = models.CharField(max_length=45)
    ocupacion = models.CharField(max_length=45)
    tipo_vivienda = models.IntegerField(choices=VIVIENDA_CHOICES, default=1)
    viajes_rec = models.CharField(max_length=50)
    conv_animales = models.CharField(max_length=50)
    heredofamiliares = models.CharField(max_length=100)
    religion = models.CharField(max_length=45)
    id_paciente_fk = models.ForeignKey(Paciente, null=True,
                                        on_delete=models.CASCADE)


class Alcoholismo(models.Model):
    id_alcoholismo = models.AutoField(primary_key=True)
    activo_alc = models.IntegerField(default=1)
    frec_dia = models.CharField(max_length=100)
    cantidad_alc = models.DecimalField(max_digits=30, decimal_places=15)
    anio_alc = models.IntegerField()
    antecedentes_personales_fk = models.ForeignKey(AntecedentesPersonales, null=True,
                                                                    on_delete=models.CASCADE)


class Tabaquismo(models.Model):
    id_tabaquismo = models.AutoField(primary_key=True)
    activo_tab = models.IntegerField(default=1)
    frec_dia_tab = models.CharField(max_length=100)
    anio_tab = models.IntegerField()
    cant_tab = models.IntegerField()    
    indice_tab = models.DecimalField(max_digits=30, decimal_places=15)
    antecedentes_personales_fk = models.ForeignKey(AntecedentesPersonales, null=True,
                                                                    on_delete=models.CASCADE)


class Droga(models.Model):
    id_droga = models.AutoField(primary_key=True)
    nom_drog = models.IntegerField(choices=DROGA_CHOICES, default=1)
    activa = models.IntegerField(choices=COMMUN_CHOICES,default=1)
    antecedentes_personales_fk = models.ForeignKey(AntecedentesPersonales, null=True,
                                                                    on_delete=models.CASCADE)
