ESTADO_CIVIL_CHOICES = (
  (1, ("Soltero")),
  (2, ("Casado")),
  (3, ("Viudo")),
  (4, ("Concubino"))
)

INSTITUCION_CHOICES = (
  (1, ("Privada")),
  (2, ("ISSSTE")),
  (3, ("IMSS")),
  (4, ("Seguro social")),
  (5, ("Seguro popular")),
  (6, ("SEDENA"))
)

SEXO_CHOICES = (
  (0, ("Masculino")),
  (1, ("Femenino"))
)

DECISION_CHOICES = (
  (0, ("No")),
  (1, ("Si"))
)

METODO_DIAG_CHOICES = (
  (1, ("Pruebas de deteccion: Prueba de Elisa")),
  (2, ("Pruebas de confirmacion: WESTERN BLOT")),
  (3, ("Pruebas de sangre o saliva: PCR"))
)

ANTICONCEPTIVO_CHOICES = (
  (1, ("Preservativo Masculino")),
  (2, ("Preservativo Femenino")),
  (3, ("Espermicida")),
  (4, ("Diafragma")),
  (5, ("Esponja Vaginal")),
  (6, ("Pildora")),
  (7, ("Minipildora Progestageno")),
  (8, ("Pildora del dia despues")),
  (9, ("Adhesivo anticonceptivo")),
  (10, ("Anillo vaginal")),
  (11, ("Anticonceptivo inyectable")),
  (12, ("Implante anticonceptivo")),
  (13, ("Vasectomia")),
  (14, ("Ligadura de trompas")),
  (15, ("Metodo del calendario menstrual")),
  (16, ("Coito interrumpido")),
  (17, ("Moco cervical")),
  (18, ("Lactancia materna"))
)

#investigar faltantes
HIST_INMUNE_CHOICES = (
  (1, ("BCG")),
  (2, ("Neumocco")),
  (3, ("Hepatitis A")),
  (4, ("Hepatitis A")),
  (5, ("Sabin o Pentavalente")),
  (6, ("Hexavalente")),
  (7, ("Influenza")),
  (8, ("Haemophilus")),
  (9, ("Tripe viral")),
  (10, ("Varicela")),
  (11, ("Rotavirus"))
)

#investigar faltantes
FACTOR_RIESGO_CHOICES = (
  (1, ("Adquisición")),
  (2, ("Prácticas sexuales")),
  (3, ("Herencia")),
  (4, ("Jeringas infectadas")),
  (5, ("Pinchazo infectado al momento de la prueba"))
)

COMMUN_CHOICES = (
  (0, ("Si")),
  (1, ("No"))
)

TELEFONO_CHOICES = (
  (0, ("Celular")),
  (1, ("Casa"))
)

HEREDOFAMILIARES_CHOICES = (
  (0, ("Diabetes")),
  (1, ("Hipertensión")),
  (2, ("Cariopatías")),
  (3, ("Osteoporosis")),
  (4, ("Tuberculosis")),
  (5, ("Cáncer Cervicouterino")),
  (6, ("Otro tipo de Cáncer")),
  (7, ("Obesidad")),
  (8, ("Dislipidemia")),
  (9, ("Insuficiencia Renal")),
  (10, ("Muerte Cardiovascular Prematura")),
  (11, ("Enfermedad Cerebrovascular")),
  (12, ("Tabaquismo")),
  (13, ("Alcoholismo")),
  (14, ("Otras Adicciones")),
  (15, ("Alergias")),
  (16, ("Tiroides")),
  (17, ("Epilepsia")),
  (18, ("Otros"))
)



VIVIENDA_CHOICES = (
    (1, ("Rural")),
    (2, ("Urbana")),
    (3, ("Otro"))
)


ESCOLARIDAD_CHOICES = (
    (1, ("Prescolar")),
    (2, ("Primaria")),
    (3, ("Secundaria")),
    (4, ("Pre-Bachillerato")),
    (5, ("Licenciatura")),
    (6, ("Maestría")),
    (7, ("Doctorado")),
    (8, ("Post-Doctorado")),
    (9, ("En curso")),
    (10, ("Terminado"))
)



DROGA_CHOICES = (
    (1, ("Marihuana")),
    (2, ("MDMA/Molly")),
    (3, ("Cocaína")),
    (4, ("Anfetaminas")),
    (5, ("Inhalables")),
    (6, ("Tranquilizantes")),
    (7, ("Éxtasis")),
    (8, ("Heroína")),
    (9, ("LSD")),
    (10, ("Setas Alucinógenas")),
    (11, ("Opio")),
    (12, ("Crack")),
    (13, ("Otro"))
)