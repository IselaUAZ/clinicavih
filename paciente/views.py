from django.shortcuts import render
from django.shortcuts import redirect
from .models import Paciente
from .forms import *
from administrador.models import Administrador
from django.utils.datetime_safe import datetime
from datetime import timezone
import string
import random
import traceback

# Create your views here.
template_folder = 'medico/'
template_administrador = 'administrador/'


###### Funcionalidad Inicial de Paciente ######
def paciente_lista(request):
    pacientes = Paciente.objects.all()
    print (pacientes)
    template_use = template_folder
    if Administrador.objects.filter(
        username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request, template_use + 'paciente_lista.html',
                  {'pacientes': pacientes, 'encabezado': 'Lista de Pacientes'})


def paciente_alta(request):
    form = RegistraPacienteForm()
    form_2 = TelefonoForm()
    form_3 = TransfusionForm()

    if request.method == 'POST':
        form = RegistraPacienteForm(data=request.POST)
        form_2 = TelefonoForm(data=request.POST)
        form_3 = TransfusionForm(data=request.POST)
        
        if form.is_valid() and form_2.is_valid() and form_3.is_valid():
            paciente = form.save()
            paciente.save()

            id = paciente.id_paciente
            paciente = form_2.save()
            paciente.id_paciente_fk = Paciente.objects.get(id_paciente=id)
            print(paciente)
            paciente.save()

            paciente = form_3.save()
            paciente.id_paciente_fk = Paciente.objects.get(id_paciente=id)
            paciente.save()

            return redirect('/paciente/antlista')


        if Administrador.objects.filter(
            username=request.user.username).count() == 1:
            template_use = template_administrador

        form = RegistraPacienteForm()
        form_2 = TelefonoForm()
        form_3 = TransfusionForm()
        return render(request, template_use + 'paciente_alta.html',{'form': form, 'form_2': form_2, 'form_3': form_3})

    return render(request,'paciente/paciente_alta.html',{'form': form, 'form_2': form_2, 'form_3': form_3})


def paciente_modifica(request, pk):
    paciente = Paciente.objects.get(pk=pk)
    #telefono = Telefono.objects.get(pk=pk)

    if request.method == 'GET':
        form = ModificaPacienteForm(instance=paciente)
        form_2 = ModificaTelefonoForm(instance=paciente)
        form_3 = ModificaTransfusionForm(instance=paciente)
    else:
        form = ModificaPacienteForm(request.POST, instance=paciente)
        form_2 = ModificaTelefonoForm(request.POST, instance=paciente)
        form_3 = ModificaTransfusionForm(request.POST, instance=paciente)
        if form.is_valid() and form_2.is_valid() and form_3.is_valid():
            paciente = form.save()
            paciente.save()

            id = paciente.id_paciente
            paciente = form_2.save()
            paciente.id_paciente_fk = Paciente.objects.get(id_paciente=id)
            paciente.save()
            paciente = form_3.save()
            paciente.id_paciente_fk = Paciente.objects.get(id_paciente=id)
            paciente.save()
            return redirect('/paciente/lista')

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request,
                  template_use + 'paciente_modifica.html', {'form': form, 'form_2': form_2, 'form_3': form_3})

###### Funcionalidad Final de Paciente ######


def antecedente_lista(request):
    pacientes = Paciente.objects.all()
    
    template_use = template_folder
    if Administrador.objects.filter(
        username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request, template_use + 'antecedentes_lista.html',
                  {'pacientes': pacientes})


def crear_antecedente(request, pk):
    paciente = Paciente.objects.get(pk=pk)

    print (paciente)

    if request.method == 'GET':
        form = AntecedentesPacienteForm()
        form_2 = HeredofamiliaresForm()

    else:    
        form = AntecedentesPacienteForm(request.POST)
        form_2 = HeredofamiliaresForm(request.POST)
        
        if form.is_valid() and form_2.is_valid():
            ant = form.save()
            ant.id_paciente_fk = Paciente.objects.get(id_paciente=paciente.id_paciente)
            ant.save()

            id = ant.id_antecedentes
            ant = form_2.save()
            ant.id_antecedentes_fk = AntecedentesPaciente.objects.get(id_antecedentes=id)
            ant.save()

            return redirect('/paciente/antecedentepatologico&id=' + str(id))

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador
    #siempre usa fuckin administrador -.-
    return render(request,
                  template_use + 'ant_paciente.html', {'form': form, 'form_2': form_2})

'''
def antecedentes_paciente(request):
    id_paciente = request.build_absolute_uri().split('%3D')[1]

    if request.method == 'GET':
        form = AntecedentesPacienteForm()
        form_2 = HeredofamiliaresForm()
    else:    
        form = AntecedentesPacienteForm(request.POST)
        form_2 = HeredofamiliaresForm(request.POST)

        if form.is_valid() and form_2.is_valid():
            ant = form.save()
            ant.id_paciente_fk = Paciente.objects.get(id_paciente=id_paciente)
            ant.save()

            #Id del antecedente creado anteriormente 
            id = ant.id_antecedentes
            ant = form_2.save()
            ant.id_antecedentes_fk = AntecedentesPaciente.objects.get(id_antecedentes=id)
            ant.save()
            return redirect('/paciente/antlista')

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request, template_use + 'ant_paciente.html', {'form': form, 'form_2': form_2})
'''

'''
def crear_antecedente_pato(request, pk):
    paciente = Paciente.objects.get(pk=pk)

    if request.method == 'GET':
        form = VacioForm(instance=paciente)
    else:
        form = VacioForm(request.POST, instance=paciente)
        if form.is_valid():
            form.save()
            print('---')
            return redirect('/paciente/antecedentepatologico&id=' + pk)

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador
    #siempre usa fuckin administrador -.-
    return render(request,
                  template_use + 'crear_pato.html', {'form': form})
'''

def antecedentes_patologicos(request):
    id_paciente = request.build_absolute_uri().split('%3D')[1]
 
    if request.method == 'GET':
        form = AntecedentesPatologicoForm()
        form_2 = HospitalizacionForm()
    else:
        form = AntecedentesPatologicoForm(request.POST)
        form_2 = HospitalizacionForm(request.POST)

        if form.is_valid() and form_2.is_valid():
            pat = form.save()
            pat.id_antecedentes_fk = AntecedentesPaciente.objects.get(id_antecedentes=id_paciente)
            pat.save()

            id = pat.id_antecedentes_pato
            pat = form_2.save()
            pat.id_antecedentes_pato_fk = AntecedentesPatologicos.objects.get(id_antecedentes_pato=id)
            pat.save()

            return redirect('/paciente/antlista')

    template_use = template_folder
    if Administrador.objects.filter(
            username=request.user.username).count() == 1:
        template_use = template_administrador

    return render(request, template_use + 'crear_pato.html', {'form': form, 'form_2':form_2})
